#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>   /* Error number definitions */
#include <pthread.h>
#include "chat.h"

void* perform_listen( void* argument )
{
  Channel_source passed_in_value = *( ( Channel_source* )argument );
  int port = passed_in_value.port;
  
  /* optionally: insert more useful stuff here */

  while(!passed_in_value.cancelation)
  {
    int attenpts = 0;
    pthread_mutex_lock(passed_in_value.lock);
    while(attenpts < 100)
    {
      int len = readSerialPort(port);
      if(len > 0)
        break;
      else
        sleep(2);
    }
    
    pthread_mutex_unlock(passed_in_value.lock);
    sleep(2);
  }
 
  return NULL;
}

int readSerialPort(int port)
{
  int n = 0,
  spot = 0;
  char buf = '\0';
  /* Whole response*/
  char response[1024];
  memset(response, '\0', sizeof response);

  do {
    n = read( port, &buf, 1 );
    if(n == 0)
      return 0;
    if(strlen(response)+n >=1023)
      break;
    sprintf( &response[spot], "%c", buf );
    spot += n;
  } while( buf != '\r' && n > 0);

  if(strlen(response))
    printf( "Other party says : %s\n", response);  
  return spot;
}