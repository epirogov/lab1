#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <pthread.h>
#include "chat.h"

#define BUFFERSIZE     256

void create_input( void* argument )
{
    Channel_source passed_in_value = *( ( Channel_source* )argument );
    int port = passed_in_value.port;
    
    /* optionally: insert more useful stuff here */
  
    while(!passed_in_value.cancelation)
    {
        pthread_mutex_lock(passed_in_value.lock);
        char *text = calloc(1,1), buffer[BUFFERSIZE];
        printf("Enter a message: \n");
        fgets(buffer, BUFFERSIZE , stdin);
        text = realloc( text, strlen(text)+1+strlen(buffer) );
        if( text )
        {
          strcat( text, buffer ); /* note a '\n' is appended here everytime */
        }

        sendMessage(port, text);
        free(text);
        pthread_mutex_unlock(passed_in_value.lock);
        sleep(2);
    }
}

int sendMessage(int port, const char* text)
{
    int n_written = 0,spot = 0;
    n_written = write(port, text, strlen(text));     
    if(n_written < 0)
    printf("Error: Message not sent \n");
}