#include <assert.h>
#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <termios.h>    // POSIX terminal control definitions
#include "chat.h"

int main( int argc, char** argv )
{
    //sudo socat -d -d PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11
    char port_name[128];
    printf( "Port name for a chat:\n" );
    if(fgets(port_name, 128 , stdin))
    {
        if(strlen(port_name)<3)
            strcpy(port_name, "ttyS1");
        else
            port_name[strlen(port_name) - 1] = 0;        
        char port[256];
        getOSSpecificPortName(port_name, port);
        printf("Port path is %s\n", port);
        init_chat(port);
        printf( "Info: All chats completed successfully\n" );
    }
    
    exit( EXIT_SUCCESS );
}