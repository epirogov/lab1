#include <pthread.h>

typedef struct Channel_source_Type {
    int  port;
    unsigned char cancelation;
    struct Channel_source_Type* oposite_channel_source;
    pthread_mutex_t* lock;
 } Channel_source;

 struct termios ajust_stdio();

 int create_channel(int port, void * perform, pthread_mutex_t* lock);

 int join_channels();

 void init_chat(const char * port);

 void* perform_listen( void* argument );

 char * getOSSpecificPortName(const char * port_name,  char * port);

 void create_input( void* argument );

 int openSerialPort(const char * port);
