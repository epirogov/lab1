#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <pthread.h>
#include "chat.h"

void init_chat(const char * port)
{
  pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

  //struct termios old_stdio = ajust_stdio();
  printf("Opening port %s\n", port);
  int serialPort = openSerialPort(port);
  char *welcome = calloc(strlen("Hi, I am connected with %s\n") + strlen(port),1);
  sprintf(welcome, "Hi, I am connected with %s\n", port);
  sendMessage(serialPort, welcome);
  free(welcome);

  printf("Info: Initiating input %s\n", port);
  //if(strcmp(port, "ttyS11") == 0)
  int result = create_channel(serialPort, create_input, &lock);
  if(result)
  {
      printf("Error: input not started\n");
  }

  printf("Info: Initiating listening %s\n", port);
  
  result = create_channel(serialPort, perform_listen, &lock);
  if(result)
  {
    printf("Error: listening not started\n");
  }
  

  join_channels();
  //tcsetattr(STDOUT_FILENO,TCSANOW,&old_stdio);
  close(serialPort);
}