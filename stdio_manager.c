#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

//this code currently not used because has not tested for linux flawors. The terminal chat opened in send-receive dialog instead.
struct termios ajust_stdio()
{
        struct termios stdio;
        struct termios old_stdio;
        
        unsigned char c='D';
        tcgetattr(STDOUT_FILENO,&old_stdio);

        memset(&stdio,0,sizeof(stdio));
        stdio.c_iflag=0;
        stdio.c_oflag=0;
        stdio.c_cflag=0;
        stdio.c_lflag=0;
        stdio.c_cc[VMIN]=1;
        stdio.c_cc[VTIME]=0;
        tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
        tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
        fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);       // make the reads non-blocking
        
        return old_stdio;
}