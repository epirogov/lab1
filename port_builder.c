char * getOSSpecificPortName(const char * port_name,  char * port)
{
    strcpy(port,  "/dev/");
    strcat(port,  port_name);
    port[strlen("/dev/") + strlen(port_name)] = 0;
}