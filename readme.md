How to use messaging app.

First of all, you need to compile a programm with your development tools like gcc and socat.

1. git clone https://epirogov@bitbucket.org/epirogov/lab1
2. cd lab1
3. gcc *.h *.c -lpthread

Still a lot of warnings here, but a.out available after build.

Second you action is for make linked virtual serial ports available in your sustem. You need to instal socat to do that. Than you can link your reserved ttys as in my example.

sudo socat -d -d PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11

Finally tou sould open two terminals to communicate.

terminal#1

>a.out
Port name for a chat:
ttyS10
...
my hello to S11!

terminal#2

>a.out
Port name for a chat:
ttyS11
...
glade to see you S10!

